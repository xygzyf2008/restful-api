package com.example.springbootdemo.exception;

/**
 * @author YF
 * @date 2022/1/1
 * @apiNote 创建自定义异常处理类：com.example.restfulDemo.BusinessException.java
 */

public class BusinessException extends RuntimeException {

    private String code;
    private String msg;

    public BusinessException(String code, String msg) {
        this.msg = msg;
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}


