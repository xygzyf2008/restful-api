package com.example.springbootdemo.exception; /**
 * @author YF
 * @date 2022/1/1
 * @apiNote 定义全局异常处理类：来处理各种的异常，包括自己定义的异常和内部的异常
 */
import com.example.springbootdemo.result.ResponseData;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalExceptionHandlerTest {

    /**
     * （1）处理自定义异常BusinessException
     */

    @ExceptionHandler(value = BusinessException.class)
    @ResponseBody
    public ResponseData bizBusinessException(BusinessException e) {
        return ResponseData.defineBusinessException(e);
    }


    /**
     * （2）处理其他的异常
     */
    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public ResponseData exceptionHandler(Exception e) {
        return new ResponseData(e);
    }
}
