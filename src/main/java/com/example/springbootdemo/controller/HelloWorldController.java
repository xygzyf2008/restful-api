package com.example.springbootdemo.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

/**
 * @author YF
 * @date 2022/1/1
 * @apiNote
 */
@RestController
@Api(tags = {"你好Controller"})
public class HelloWorldController {
    @ApiOperation(value = "hello", notes = "notes ")
    @RequestMapping("/hello")
    public String hello(@ApiParam("用户对象") @PathVariable int id) throws Exception {
        return "HelloWorld ,Spring Boot!" + id;
    }

    //使用该注解忽略这个API
    @ApiIgnore
    @RequestMapping(value = "/ignoreApi")
    public String ignoreApi() {
        return "HelloWorld ,Spring Boot!";
    }
}
