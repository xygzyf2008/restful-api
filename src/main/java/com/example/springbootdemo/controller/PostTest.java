package com.example.springbootdemo.controller;

import com.example.springbootdemo.entity.Article;
import com.example.springbootdemo.entity.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.net.URI;

/**
 * @author YF
 * @date 2022/1/11
 * @apiNote
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class PostTest {
    @Autowired
    RestTemplateBuilder restTemplateBuilder;
    RestTemplate restTemplate = new RestTemplate();

    // 使用postForEntity
    @Test
    public void postForEntity() {
        //RestTemplate client= restTemplateBuilder.build();
        // 封装参数，千万不要替换为Map与HashMap，否则参数无法传递
        MultiValueMap<String, Object> paramMap = new LinkedMultiValueMap<String, Object>();
        paramMap.add("name", "longzhiran");
        paramMap.add("id", 4);
        /* User user = new User();
        user.setName("hongwei");
        user.setId(4);*/
        //方法的第一参数表示要调用的服务的地址
        //方法的第二个参数表示上传的参数
        //方法的第三个参数表示返回的消息体的数据类型
        ResponseEntity<User> responseEntity = restTemplate.postForEntity("http://localhost:8080/postuser", paramMap, User.class);
        System.out.println(responseEntity.getBody().getName());
    }

    // 使用postForObject
    @Test
    public void postForObject() {
        // 封装参数，千万不要替换为Map与HashMap，否则参数无法传递
        MultiValueMap<String, Object> paramMap = new LinkedMultiValueMap<String, Object>();
        paramMap.add("name", "longzhonghua");
        paramMap.add("id", 4);
        RestTemplate client = restTemplateBuilder.build();
        String response = client.postForObject("http://localhost:8080/postuser", paramMap, String.class);
        System.out.println(response);
    }

    // 使用postForexchange
    @Test
    public void postForexchange() {
        // 封装参数，千万不要替换为Map与HashMap，否则参数无法传递
        MultiValueMap<String, Object> paramMap = new LinkedMultiValueMap<String, Object>();
        paramMap.add("name", "longzhonghua");
        paramMap.add("id", 4);
        RestTemplate client = restTemplateBuilder.build();
        HttpHeaders headers = new HttpHeaders();
        //headers.set("id", "long");
        HttpEntity<MultiValueMap<String, Object>> httpEntity = new HttpEntity<MultiValueMap<String, Object>>(paramMap, headers);
        ResponseEntity<String> response = client.exchange("http://localhost:8080/postuser", HttpMethod.POST, httpEntity, String.class, paramMap);
        System.out.println(response.getBody());
    }
    // 使用postForLocation
    @Test
    public void postForLocation() {
        // 封装参数，千万不要替换为Map与HashMap，否则参数无法传递
        MultiValueMap<String, Object> paramMap = new LinkedMultiValueMap<String, Object>();
        paramMap.add("name", "longzhonghua");
        paramMap.add("id", 4);

        RestTemplate client = restTemplateBuilder.build();

        URI response = client.postForLocation("http://localhost:8080/post",paramMap);

        System.out.println(response);
    }

    // 使用 DELETE方法去删除资源
    @Test
    public void testDelete()  {
        String url = "http://jsonplaceholder.typicode.com/posts/1";
        restTemplate.delete(url);
    }

    @Test
    public void testPut()  {
        // 请求地址
        String url = "http://jsonplaceholder.typicode.com/posts/1";

        // 要发送的数据对象（修改数据）
        Article postDTO = new Article();
        postDTO.setId(110);
        postDTO.setTitle("zimug 发布文章");
        postDTO.setBody("zimug 发布文章 测试内容");

        // 发送PUT请求
        restTemplate.put(url, postDTO);
    }
}
