package com.example.springbootdemo.controller;
import com.example.springbootdemo.entity.Article;
import com.example.springbootdemo.repository.ArticleRepository;
import com.example.springbootdemo.result.Response;
import com.example.springbootdemo.result.ResponseData;
import com.example.springbootdemo.result.ExceptionMsg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author YF
 * @date 2022/1/1
 * @apiNote
 */
/**
 * 文章接口类
 */
@RequestMapping("/article")
@RestController
public class ArticleController {

    protected Response result(ExceptionMsg msg){
        return new Response(msg);
    }
    protected Response result(){
        return new Response();
    }

    @Autowired
    private ArticleRepository articleRepository;


    /**
     * 查询所有文章
     * http://localhost:8080/article/
     * @return
     */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ResponseData getArticleList() {
        List<Article> list = new ArrayList<Article>(articleRepository.findAll());
        return new ResponseData(ExceptionMsg.SUCCESS,list);

    }

    /**
     * 增加文章信息
     * http://localhost:8080/article/
     * {
     *     "id": 5,
     *     "title": "555",
     *     "body": "555"
     * }
     * @param article  文章对象
     * @return
     */
    @RequestMapping(value = "/", method = RequestMethod.POST)
    public ResponseData add(@RequestBody Article article) {
        articleRepository.save(article);
        return new ResponseData(ExceptionMsg.SUCCESS,article);
    }


    /**
     * 删除文章信息
     * http://localhost:8080/article/5
     * @param id  文章id
     * @return  Response
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public Response delete(@PathVariable("id") int id) {
        articleRepository.deleteById(id);
        return result(ExceptionMsg.SUCCESS);

    }


    /**
     * 修改文章信息
     * @param model  文章对象
     * @return ResponseData
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseData update(@RequestBody Article model) {
        articleRepository.save(model);
        return new ResponseData(ExceptionMsg.SUCCESS,model);
    }

    /**
     * 根据文章id 查询文章信息
     * @param id
     * @return ResponseData
     * @throws IOException
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseData findArticle(@PathVariable("id") int id) throws IOException {
        Article article = articleRepository.findById(id);
        if (article != null) {
            return new ResponseData(ExceptionMsg.SUCCESS,article);
        }
        return new ResponseData(ExceptionMsg.FAILED,article);
    }

}

