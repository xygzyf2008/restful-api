package com.example.springbootdemo.controller; /**
 * @author YF
 * @date 2022/1/1
 * @apiNote 统一异常测试类
 */

import com.example.springbootdemo.entity.User;
import com.example.springbootdemo.exception.BusinessException;
import com.example.springbootdemo.result.ResponseData;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ValidationException;

@RestController
// @RequestMapping("/test")
public class TestController {

    /**
     * 自定义异常测试
     *
     * @return
     */
    @RequestMapping("/businessException")
    public String testResponseStatusExceptionResolver(@RequestParam("i") int i) {
        if (i == 0) {
            throw new BusinessException("600", "自定义业务错误");
        }
        throw new ValidationException();
    }


    /**
     * 处理其他的异常
     *
     * @return
     */
    @RequestMapping("/getException")
    public ResponseData Exception() {

        ResponseData responseData = new ResponseData();
        responseData.setRspCode("400");
        responseData.setRspMsg("出错");

        return responseData;
    }

    /**
     * 创建测试的API
     *
     * @param user
     * @return
     */

    @RequestMapping(value = "/getparameter", method = RequestMethod.GET)
    public User getparameter(User user) {
        return user;
    }

    @RequestMapping(value = "/getuser1", method = RequestMethod.GET)
    public User user1() {
        return new User(1, "zhonghua");
    }

    @RequestMapping(value = "/postuser", method = RequestMethod.POST)
    public User postUser(User user) {
        System.out.println("name:" + user.getName());
        System.out.println("id:" + user.getId());
        return user;
    }

    @ResponseBody
    @RequestMapping(path = "success")
    public String loginSuccess(String name, Integer id) {
        return "welcome " + name;
    }

    @RequestMapping(value = "/post", method = RequestMethod.POST)
    public String post(HttpServletRequest request, @RequestParam(value = "name", required = false) String name, @RequestParam(value = "password", required = false) String password, @RequestParam(value = "id", required = false) Integer id, HttpServletResponse response) {
        // 如果获取的值为“null”，则需要把URI添加到response信息的header中。添加方法为：“response.addHeader("Location",uri)”
        response.addHeader("Location", "success?name=" + name + "&id=" + id + "&status=success");
        return "redirect:/success?name=" + name + "&id=" + id + "&status=success";
        // return "redirect:/success?name=" + URLEncoder.encode(name, "UTF-8") + "&id=" + id + "&status=success";
    }
}

