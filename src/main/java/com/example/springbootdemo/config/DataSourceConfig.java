package com.example.springbootdemo.config;

import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceBuilder;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;

/**
 * @Program: css-java
 * @Description: 多数据源配置
 * @Author: Yuan Fang
 * @Create: 2019-09-09 15:28:24
 **/
@Configuration
public class DataSourceConfig {
    @Primary
    @Bean(name = "mysqlDatasource")
    @ConfigurationProperties("spring.datasource.druid.mysql")
    public DataSource dataSourceOne() {
        return DruidDataSourceBuilder.create().build();
    }

    @Bean(name = "mysqlJdbcTemplate")
    public JdbcTemplate primaryJdbcTemplate(
            @Qualifier("mysqlDatasource") DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }

    /*@Bean(name = "fwDatasource")
    @ConfigurationProperties("spring.datasource.druid.fw")
    public DataSource fwDatasource() {
        return DruidDataSourceBuilder.create().build();
    }*/

    /*@Bean(name = "fwJdbcTemplate")
    public JdbcTemplate fwJdbcTemplate(
            @Qualifier("fwDatasource") DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }*/


}
