package com.example.springbootdemo.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

/**
 * @author YF
 * @date 2022/1/1
 * @apiNote
 */
/*@Configuration
public class RestTemplateConfig {
    @Bean
    @Primary
    public RestTemplate restTemplate() {
        HttpComponentsClientHttpRequestFactory httpRequestFactory = new HttpComponentsClientHttpRequestFactory();
        httpRequestFactory.setConnectionRequestTimeout(300000);    //连接请求超时时间
        httpRequestFactory.setConnectTimeout(300000);              //连接超时时间
        httpRequestFactory.setReadTimeout(300000);                 //读取超时时间
        return new RestTemplate(httpRequestFactory);
    }
}*/

