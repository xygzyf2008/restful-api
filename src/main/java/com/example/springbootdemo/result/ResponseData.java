package com.example.springbootdemo.result;

import com.example.springbootdemo.exception.BusinessException;

/**
 * @author YF
 * @date 2022/1/1
 * @apiNote 返回结果数据格式封装
 */
public class ResponseData extends Response {
    private Object data;


    public ResponseData() {

    }

    public ResponseData(Object data) {
        this.data = data;
    }

    public ResponseData(ExceptionMsg msg) {
        super(msg);
    }

    public ResponseData(String rspCode, String rspMsg) {
        super(rspCode, rspMsg);
    }

    public ResponseData(String rspCode, String rspMsg, Object data) {
        super(rspCode, rspMsg);
        this.data = data;
    }

    public ResponseData(ExceptionMsg msg, Object data) {
        super(msg);
        this.data = data;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }


    /**
     * 自定义异常的返回结果
     *
     * @param de
     * @return
     */
    public static ResponseData defineBusinessException(BusinessException de) {
        ResponseData responseData = new ResponseData();
        responseData.setRspCode(de.getCode());
        responseData.setRspMsg(de.getMsg());
        responseData.setData(null);
        return responseData;
    }
}

