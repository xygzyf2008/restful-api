package com.example.springbootdemo.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.*;

/**
 * @author YF
 * @date 2022/1/1
 * @apiNote
 */
@Entity
@Table(name = "article")
@Data
@ApiModel("文章实体类")
public class Article {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty("id")
    private long id;
    @ApiModelProperty("标题")
    private String title;
    private String body;
}
