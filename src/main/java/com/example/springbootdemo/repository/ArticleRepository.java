package com.example.springbootdemo.repository;

/**
 * @author YF
 * @date 2022/1/1
 * @apiNote
 */

import com.example.springbootdemo.entity.Article;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ArticleRepository extends JpaRepository<Article, Long>, JpaSpecificationExecutor<Article> {
    Article findById(long id);

    void deleteById(long id);
}
